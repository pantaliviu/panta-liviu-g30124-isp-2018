package g30124.panta.liviu.lab6.ex3;

import java.awt.Color;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,50,50,"Cerc1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,180,50,"Cerc2",true);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE, 150,200,80,250,"Dreptunghi",true);
        b1.addShape(s3);
      //  b1.DeleteById("Cerc1");
    }
}