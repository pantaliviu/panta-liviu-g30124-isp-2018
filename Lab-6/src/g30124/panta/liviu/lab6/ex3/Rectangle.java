package g30124.panta.liviu.lab6.ex3;

import java.awt.Color;
import java.awt.Graphics;

public class Rectangle implements Shape{

    private int length;
    private int width;
    private Color color;
    private int x;
    private int y;
    private String id;
    private boolean fill;

    public Rectangle(Color color, int length, int width, int x, int y, String id,boolean fill) {
        this.color = color;
        this.length = length;
        this.width = width;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
    }
    
    public Color getColor() {
    	return color;
    }
    
    public boolean isFill() {
    	return fill;
    }
    
    public String getId() {
    	return id;
    }

    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill() == true)
        	g.fillRect(x,y,length, width);
        else
        	g.drawRect(x,y,length,width);
    }
}
