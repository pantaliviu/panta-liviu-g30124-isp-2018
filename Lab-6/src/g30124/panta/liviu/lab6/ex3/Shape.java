package g30124.panta.liviu.lab6.ex3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
    void draw(Graphics g);

	String getId();
	
	Color getColor();
}