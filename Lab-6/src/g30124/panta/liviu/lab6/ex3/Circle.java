package g30124.panta.liviu.lab6.ex3;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape{

    private int radius;
    private int x;
    private int y;
    private boolean fill;
    private String id;
    private Color color;

    public Circle(Color color, int radius,int x, int y,String id,boolean fill) {
        this.radius = radius;
        this.color = color;
        this.x = x;
        this.y = y;
        this.id = id;
        this.fill = fill;
    }

    public int getRadius() {
        return radius;
    }
    
    public Color getColor() {
    	return color;
    }
    
    public String getId() {
    	return id;
    }
    
    public boolean isFill() {
    	return fill;
    }

    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill() == true)
        	 g.fillOval(x,y,radius, radius);
        else
        	 g.drawOval(x,y,radius,radius);
    }
}