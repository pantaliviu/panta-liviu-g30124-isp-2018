package g30124.panta.liviu.lab6.ex4;

import java.util.Arrays;

public class Sequence implements CharSequence {

	private char[] text;

    public Sequence(char[] sir) {
        this.text = sir;
    }

    @Override
    public int length() {
        return text.length;
    }

    @Override
    public char charAt(int index) {
        return text[index];
    }

    @Override
    public CharSequence subSequence(int from, int to) {
        char[] cuv = Arrays.copyOfRange(text,from,to);
        return new Sequence(cuv);
    }
    
    public static void main(String[] args) {
    	String str = "Ana are mere";
    	char[] sir = str.toCharArray();
    	Sequence newchar = new Sequence(sir);
    	System.out.println("Litera de la pozitia dorita este: " +newchar.charAt(10));
    	System.out.println("Lungimea sirului este: " +newchar.length());
    	System.out.println("Subsirul ales este: " +newchar.subSequence(4,7));
    	}

}
