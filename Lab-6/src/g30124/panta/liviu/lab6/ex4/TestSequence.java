package g30124.panta.liviu.lab6.ex4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestSequence {

	@Test
	public void testCharAt() {
		String str1 = "Ana are mere";
    	char[] sir1 = str1.toCharArray();
    	Sequence newchar1 = new Sequence(sir1);
    	String str2 = "porumbel";
    	char[] sir2 = str2.toCharArray();
    	Sequence newchar2 = new Sequence(sir2);
    	assertEquals(newchar1.charAt(5),newchar2.charAt(2));
	}
	
	@Test
	public void testLength() {
		String str1 = "scaun";
		char[] sir1 = str1.toCharArray();
		Sequence newchar1 = new Sequence(sir1);
		String str2 = "birou";
		char[] sir2 = str2.toCharArray();
		Sequence newchar2 = new Sequence(sir2);
		assertEquals(newchar1.length(),newchar2.length());
	}
	
	@Test
	public void testSubSequence() {
		String str = "porumbel";
		char[] sir = str.toCharArray();
		Sequence newchar = new Sequence(sir);
		assertTrue(newchar.subSequence(0,3) == "por");
	}
}
