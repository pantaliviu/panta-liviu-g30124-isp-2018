package g30124.panta.liviu.lab6.ex1;

import java.awt.*;

public abstract class Shape {

    private Color color;
    protected int x;
    protected int y;
    private boolean fill;
    protected String id;

    public Shape(Color color,int x, int y,String id,boolean fill) {
        this.color = color;
        this.fill = fill;
        this.x = x;
        this.y = y;
        this.id = id;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
    
    public String getId() {
    	return id;
    }
    
    public boolean isFill() {
    	return fill;
    }

    public abstract void draw(Graphics g);
}
