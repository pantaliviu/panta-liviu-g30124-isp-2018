package g30124.panta.liviu.lab6.ex1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(Color color, int length, int width, int x, int y, String id,boolean fill) {
        super(color, x ,y,id,fill);
        this.length = length;
        this.width = width;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if(isFill() == true)
        	g.fillRect(x,y,length, width);
        else
        	g.drawRect(x,y,length,width);
    }
}
