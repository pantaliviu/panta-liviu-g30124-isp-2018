package g30124.panta.liviu.lab6.ex5;

import java.awt.Graphics;

public interface Shape {

	void draw(Graphics g);
}
