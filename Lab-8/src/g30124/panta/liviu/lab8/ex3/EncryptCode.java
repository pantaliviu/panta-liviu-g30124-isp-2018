package g30124.panta.liviu.lab8.ex3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class EncryptCode {

	StringBuilder enc = new StringBuilder();
	
	public void encrypt(String f) throws FileNotFoundException {
		for(int i=0;i < f.length(); i++) {
			int caracter = (int)f.charAt(i);
			caracter++;
			enc.append((char) caracter);
		}
		f = enc.toString();
		System.out.println(f);
		PrintWriter pw = new PrintWriter("E:\\Programe AC\\Java\\Lab-8\\src\\g30124\\panta\\liviu\\lab8\\ex3\\datacrypt.enc");
	}
	
	public void decrypt(String f) throws FileNotFoundException {
		for(int i=0;i<f.length();i++) {
			int caracter = (int)f.charAt(i);
			caracter=caracter-1;
			enc.append((char) caracter);
		}
		f = enc.toString();
		System.out.print(f);
		PrintWriter pw = new PrintWriter("E:\\Programe AC\\Java\\Lab-8\\src\\g30124\\panta\\liviu\\lab8\\ex3\\datacrypt.dec");
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
		File file = new File("E:\\Programe AC\\Java\\Lab-8\\src\\g30124\\panta\\liviu\\lab8\\ex3\\datacrypt.txt");
		Scanner en = new Scanner(file);
		String crypt = en.next();
		en.close();
		System.out.println("Fisierul contine textul: "+crypt);
		EncryptCode ec = new EncryptCode();
		ec.encrypt(crypt);
		ec.decrypt(crypt);
		}
}
