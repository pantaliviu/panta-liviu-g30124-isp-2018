package g30124.panta.liviu.lab8.ex4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Car implements Serializable{

	private String model;
	private int price;
	
	public Car(String model, int price) {
		this.model = model;
		this.price = price;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public static void main(String[] args) throws Exception {
		
		Car car1 = new Car("Audi", 30000);
		Car car2 = new Car("Mercedes", 50000);
		Car car3 = new Car("Bentley", 100000);
		
        FileOutputStream fos = new FileOutputStream("E:\\\\Programe AC\\\\Java\\\\Lab-8\\\\src\\\\g30124\\\\panta\\\\liviu\\\\lab8\\\\ex4\\\\CarGarage.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(car1);
        oos.writeObject(car2);
        oos.writeObject(car3);
        fos.close();
        
		FileInputStream fis = new FileInputStream("E:\\Programe AC\\Java\\Lab-8\\src\\g30124\\panta\\liviu\\lab8\\ex4\\CarGarage.txt");
		ObjectInputStream oin = new ObjectInputStream(fis);
		Car car4 = (Car)oin.readObject();
		Car car5 = (Car)oin.readObject();
		Car car6 = (Car)oin.readObject();
		fis.close();
		System.out.println(car4.getModel()+" "+car4.getPrice());
		System.out.println(car5.getModel()+" "+car5.getPrice());
		System.out.println(car6.getModel()+" "+car6.getPrice());
		}
}
