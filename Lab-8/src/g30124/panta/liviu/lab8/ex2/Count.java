package g30124.panta.liviu.lab8.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Count {

	public static void main(String[] args) throws FileNotFoundException {
		
		int count=0;
		File file = new File("E:\\Programe AC\\Java\\Lab-8\\src\\g30124\\panta\\liviu\\lab8\\ex2\\Litere.txt");
		Scanner lit = new Scanner(file);
		String f = lit.next();
		lit.close();
		System.out.println("Fisierul contine textul: "+f);
		Scanner g = new Scanner(System.in);
		System.out.println("Introduceti litera pe care doriti sa o numarati: ");
		char litera = g.next().charAt(0);
		g.close();
		for(int i=0;i < f.length(); i++) {
			if(f.charAt(i) == litera)
				count++;
		}
		System.out.println("Litera "+litera+" a fost gasita in fisier de "+count+" ori.");
	}
}
