package g30124.panta.liviu.lab7.ex2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import g30124.panta.liviu.lab7.ex1.BankAccount;

public class Bank extends BankAccount{

	static ArrayList<BankAccount> acc = new ArrayList<BankAccount>();
	
	public Bank(String owner, double balance) {
		super(owner,balance);
	}
	
	public void addAccount(String owner, double balance) {
		Bank b = new Bank(owner,balance);
		acc.add(b);
	}
	
	public void printAccounts() {
		for(int i=0;i<acc.size();i++) {
			System.out.println(acc.get(i).getBalance()+" "+acc.get(i).getOwner());
		}
	}
	
	public BankAccount getAccount(String owner) {
		for(int i=0;i<acc.size();i++) {
			if(acc.get(i).getOwner().equals(owner))
				System.out.println(acc.get(i).getBalance()+" "+acc.get(i).getOwner());
			//	return acc.get(i);
		}
		return null;
	}
	
	public void printAccount() {
		Collections.sort(acc);
		System.out.println("Lista sortata dupa balance este: ");
		for(BankAccount i:acc)
			System.out.println(i.getBalance()+" "+i.getOwner());
	}
	public void printAccounts(int min, int max) {
		for(BankAccount i:acc) {
			if(i.getBalance()>min && i.getBalance()<max)
				System.out.println("Numele contului este: "+i.getOwner()+" cu valoarea de "+i.getBalance());
		}
	}
	
	public static void main(String[] args) {

		Bank b = new Bank("Liviu",155);
		b.addAccount("Liviu",18451);
		b.addAccount("Lavinia", 23083);
		b.addAccount("Simona", 15624);
		b.addAccount("Ana", 16468);
		b.printAccount();
		System.out.println("========================================");
		b.getAccount("Liviu");
		System.out.println("---------------------------------------");
		b.printAccounts(10000, 24000);
		System.out.println("...............................");
		Collections.sort(acc, new Comparator<BankAccount>() {
		    public int compare(BankAccount one, BankAccount other) {
		        return one.getOwner().compareTo(other.getOwner());
		    }
		});
		for(BankAccount i:acc) {
			System.out.println(i.getOwner()+" "+i.getBalance());
		}
	}
}
