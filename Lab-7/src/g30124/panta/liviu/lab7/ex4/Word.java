package g30124.panta.liviu.lab7.ex4;

import java.util.Objects;

public class Word {

	protected String word;
	
	public Word(String w) {
		this.word = w;
	}
	
	public void setWord(String word) {
		this.word = word;
	}
	
	 @Override
	    public String toString() {
	        return " Word: " + word ;
	    }
	 
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	        Word cuv = (Word) o;
	        return Objects.equals(word, cuv.word);
	    }
	 
	 @Override
	    public int hashCode() {

	        return Objects.hash(word);
	    }
}
