package g30124.panta.liviu.lab7.ex4;

public class Definition {

	protected String description;
	
	public Definition(String def) {
		this.description = def;
	}
	
	public void setDescription(String def) {
		this.description = def;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	@Override
    public String toString() {
        return " Definition "+ description;
    }
}
