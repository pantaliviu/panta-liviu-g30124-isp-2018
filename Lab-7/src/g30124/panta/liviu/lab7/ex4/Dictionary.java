package g30124.panta.liviu.lab7.ex4;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Dictionary {

	HashMap<Word,Definition> dex = new HashMap<Word,Definition>();
	
	public void addWord(Word w, Definition d) {
		if(dex.containsKey(w))
			System.out.println("Acest cuvant exista deja");
		else
			dex.put(w, d);
	}
	
	public Definition getDefinition(Word w) {
		System.out.println(dex.containsKey(w));
		return (Definition) dex.get(w);
	}
	
	public void getAllWord() {
		
		dex.forEach((key,value) -> System.out.println(key));
	}
	
	public void getAllDefinitions() {
		dex.forEach((key,value) -> System.out.println(value));
	}
	
	@Override
    public String toString() {
        return "Dictionary{" +
                "dex=" + dex +
                '}';
    }
	
	public static void main(String[] args) {
		
		Dictionary d = new Dictionary();
		Definition def = new Definition("Is a fruit");
		Definition def1 = new Definition("Is an animal");
		Word w = new Word("apple");
		Word w1 = new Word("horse");
		d.addWord(w, def);
		d.addWord(w1, def1);
		d.getAllWord();
		System.out.println(d.getDefinition(w1));
	}
}
