package g30124.panta.liviu.lab7.ex3;

public class BankAccount implements Comparable<BankAccount>{

	protected String owner;
	protected double balance;
	
	public BankAccount(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}
	
	public void withdraw(double amount) {
		this.balance = this.balance - amount;
	}
	
	public void deposit(double amount) {
		this.balance = this.balance + amount;
	}
	
	public String getOwner() {
		return this.owner;
	}
	
	public double getBalance() {
		return this.balance;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount) {
			if(this.owner.equals(((BankAccount) obj).getOwner()) && this.balance == ((BankAccount)obj).getBalance())
				return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (int) (balance + owner.hashCode());
	}
	
	public int compareTo(BankAccount a) {
		if(balance>a.getBalance()) 
			return 1;
		if(balance==a.getBalance())
			return 0;
		return -1;
	}
	
	public static void main(String[] args) {
		
		BankAccount b1 = new BankAccount("Liviu",1000);
		BankAccount b2 = new BankAccount("Liviu",1000);
		System.out.println(b1.equals(b2));
		b2.withdraw(100);
		System.out.println(b2.equals(b1));
	}
}
