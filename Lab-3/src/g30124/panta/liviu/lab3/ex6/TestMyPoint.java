package g30124.panta.liviu.lab3.ex6;

public class TestMyPoint {
    public static void main (String[] args){
        MyPoint a = new MyPoint();
        MyPoint b = new MyPoint(1,2);
        a.setX(1);
        a.setY(1);
        b.setXY(2,1);
        System.out.println("To String de a este: " + a.toString());
        System.out.println("Distanta de la a la b este: " + a.distance(b));
        System.out.println("Distanta de la a la b cu coordonate este: " + a.distance(b.getX(),b.getY()));
    }
}
