package g30124.panta.liviu.lab3.ex3;

import becker.robots.*;

public class NorthMove {
	
	public static void main(String[] args)
	{
		City ny = new City();
		Robot kevin = new Robot(ny,1,1,Direction.NORTH);
		
		
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.move();
	}

}
