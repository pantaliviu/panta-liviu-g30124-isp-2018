//acesta este programul pentru testul de la laborator

package g30124.panta.liviu.lab3.test;

public class Unpaired {
	
	
	 public static int solution(int[] A){

	    	int i=0,x=0;
	    	while(i<A.length)
	    	{
	    		int ok=0;
	    		for(int j=1;j<A.length;j++)
	    		{
	    			if(A[i]==A[j])
	    				ok=1;
	    		if(ok==0)
	    		   x = A[i];
	    		}
	    		i++;
	    	}
	    	return x;
	    }

	    public static void main(String[] args) {
	        int[] A = new int[7];
	        A[0] = 9;  A[1] = 3;  A[2] = 9;
	        A[3] = 3;  A[4] = 9;  A[5] = 3;
	        A[6] = 7;
	        int result = solution(A);
	        System.out.println(result);
	        if(result==7)
	            System.out.println("Rezultat corect.");
	        else
	            System.out.println("Rezultat incorect.");
	    }
}
