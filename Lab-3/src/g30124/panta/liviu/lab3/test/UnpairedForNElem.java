//Acesta este acelasi program de determinare doar ca este pentru un vector cu n elemente

package g30124.panta.liviu.lab3.test;

import java.util.*;

public class UnpairedForNElem {

	public static int solution(int[] A){

    	int i=0,x=0;
    	while(i<A.length)
    	{
    		int ok=0;
    		for(int j=1;j<A.length;j++)
    		{
    			if(A[i]==A[j])
    				ok=1;
    		if(ok==0)
    		   x = A[i];
    		}
    		i++;
    	}
    	return x;
    }
	
	public static void main(String[] args)
	{
		int n;
		Scanner in = new Scanner(System.in);
		System.out.println("Dati numarul de elemente: ");
		n = in.nextInt();
		System.out.println("Introduceti elementele pentru vector: ");
		int[] A = new int[n];
		for(int i=0;i<n;i++)
			{ A[i] = in.nextInt(); }
		int result = solution(A);
		System.out.println("Numarul fara pereche este: " +result);
		in.close();
	}
}
