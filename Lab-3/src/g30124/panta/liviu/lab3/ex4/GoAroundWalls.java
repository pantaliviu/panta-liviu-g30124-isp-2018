package g30124.panta.liviu.lab3.ex4;

import becker.robots.*;

public class GoAroundWalls {
	
	public static void main(String[] args)
	{
		City ny = new City();
		Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
		Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
		Wall blockAve2 = new Wall(ny, 2, 1, Direction.SOUTH);
		Wall blockAve3 = new Wall(ny, 2, 2, Direction.SOUTH);
		Wall blockAve4 = new Wall(ny, 2, 2, Direction.EAST);
		Wall blockAve5 = new Wall(ny, 1, 2, Direction.EAST);
		Wall blockAve6 = new Wall(ny, 1, 1, Direction.NORTH);
		Wall blockAve7 = new Wall(ny, 1, 2, Direction.NORTH);
		Robot kevin = new Robot(ny, 0, 2, Direction.WEST);
		
		
		kevin.move();
		kevin.move();
		kevin.turnLeft();
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.turnLeft();
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.turnLeft();
		kevin.move();
		kevin.move();
		kevin.move();
		kevin.turnLeft();
		kevin.move();
		
	}

}
