package g30124.panta.liviu.lab3.ex5;

import becker.robots.*;

public class FetchingTheNewspaper {
	
	public static void main(String[] args)
	{
		City ny = new City();
		Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
		Wall blockAve1 = new Wall(ny, 2, 1, Direction.WEST);
		Wall blockAve2 = new Wall(ny, 2, 1, Direction.SOUTH);
		Wall blockAve3 = new Wall(ny, 1, 1, Direction.NORTH);
		Wall blockAve4 = new Wall(ny, 1, 2, Direction.NORTH);
		Wall blockAve5 = new Wall(ny, 1, 2, Direction.EAST);
		Wall blockAve6 = new Wall(ny, 1, 2, Direction.SOUTH);
		Robot kevin = new Robot(ny, 1, 2, Direction.SOUTH);
		Thing paper = new Thing(ny, 2, 2);
		
		
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.move();
		kevin.turnLeft();
		kevin.move();
		kevin.turnLeft();
		kevin.move();
		kevin.pickThing();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.move();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.move();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.move();
		kevin.turnLeft();
		kevin.turnLeft();
		kevin.turnLeft();
		
		
	}

}
