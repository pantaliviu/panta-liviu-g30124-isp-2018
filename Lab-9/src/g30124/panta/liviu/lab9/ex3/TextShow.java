package g30124.panta.liviu.lab9.ex3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;

public class TextShow extends JFrame implements ActionListener{

	JButton Cauta;
	JTextField Text;
	JTextField Afis;
	
	TextShow(){
		setTitle("Afiseaza fisier");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		setSize(300,300);
		setVisible(true);
	}
	
	public void init() {
		
		this.setLayout(null);
		
		Cauta = new JButton("Cauta");
		Cauta.setBounds(100, 40, 80, 20);
		Cauta.addActionListener(this);
		
		Text = new JTextField();
		Text.setBounds(10,10,265,20);
		
		Afis = new JTextField();
		Afis.setBounds(10, 70, 265, 185);
		Afis.setEnabled(false);
		
		add(Text); add(Cauta); add(Afis);
	}
	
	public void actionPerformed(ActionEvent c) {
		String str = new String();
		if(c.getSource() == Cauta)
			str = Text.getText();
			try {
				Afis.setText(cautaFisier(str));
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public String cautaFisier(String fis) throws IOException {
		
		BufferedReader in = new BufferedReader(new FileReader(fis));
		String line;
		while((line = in.readLine()) != null) {
			return line;
		}
		in.close();
		return null;
	}
	
	public static void main(String[] args) {
		new TextShow();
	}

}
