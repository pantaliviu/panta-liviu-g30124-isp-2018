package g30124.panta.liviu.lab9.ex2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class IncrementField extends JFrame implements ActionListener{

	private int x=0;
	
	JTextField Count;
	JButton Increment;
	JButton Reset;
	
	IncrementField(){
		setTitle("Count Button");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,115);
        setVisible(true);
		}
	
	public void init() {
		
		this.setLayout(null);
		int width=80; int height = 20;
		
		Count = new JTextField();
		Count.setBounds(10, 10, 160, height);
		Count.setEnabled(false);
		
		Increment = new JButton("Count");
		Increment.setBounds(10, 40, width, height);
		Increment.addActionListener(this);
		
		Reset = new JButton("Reset");
		Reset.setBounds(90, 40, width, height);
		Reset.addActionListener(this);
		
		Count.setText("Number: "+x);
		
		add(Count); add(Increment); add(Reset);	}
	


	public void actionPerformed(ActionEvent c) {
		
		if(c.getSource() == Increment) {
			x++;
			Count.setText("Number: "+x);
		}
		
		if(c.getSource() == Reset) {
			x=0;
			Count.setText("Number: "+x);
		}
		
		}
	
	public static void main(String[] args) {
		new IncrementField();
	}

}
