package g30124.panta.liviu.lab5.ex1;

public class Rectangle extends Shape{

	protected double width;
	protected double length;
	
	public Rectangle() {
		this.width = 2.0;
		this.length = 4.0;
	}
	
	public Rectangle(double width, double length) {
		this.width = width;
		this.length = length;
	}
	
	public Rectangle(double width, double length, String color, boolean filled) {
		super(color,filled);
		this.width = width;
		this.length = length;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public double getLength() {
		return length;
	}
	
	public void setLength(double length) {
		this.length = length;
	}
	
	public double getArea() {
		return this.width*this.length;
	}
	
	public double getPerimeter() {
		return 2*this.width+2*this.length;
	}
	
	@Override
	public String toString() {
		return new String("Rectangle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               ", width=" + width +", length="+length+
	               '}');
	}
	
	public static void main(String[] args) {
		Rectangle r = new Rectangle(2.0,4.0,"gry",true);
		r.setLength(5.0);
		r.setWidth(3.0);
		System.out.println(r.toString());
		System.out.println("Area of rectangle is: " +r.getArea());
		System.out.println("Perimeter of rectangle is: " +r.getPerimeter());
		System.out.println("Width of rectangle is: " +r.getWidth());
		System.out.println("Length of rectangle is:" +r.getLength());
	}
}
