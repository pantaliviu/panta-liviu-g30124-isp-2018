package g30124.panta.liviu.lab5.ex1;

public class Circle extends Shape{

	 protected double radius;

	   public Circle(){
		   	this.radius = 3.0;
	   }

	   public Circle(double radius) {
	       this.radius = radius;
	   }

	   public Circle(String color, boolean filled, double radius) {
	       super(color, filled);
	       this.radius = radius;
	   }

	   public double getRadius() {
	       return radius;
	   }

	   public void setRadius(double radius) {
	       this.radius = radius;
	   }

	   public double getArea() {
	       return 3.14*this.radius*this.radius;
	   }

	   public double getPerimeter() {
	       return 2*3.14*this.radius;
	   }

	   @Override
	   public String toString() {
	       return "Circle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled +
	               ", radius=" + radius +
	               '}';
	   }
	   
	   public static void main(String[] args) {
		   Circle c = new Circle("blue",true,3.0);
		   System.out.println(c.toString());
		   System.out.println("Area of circle is: " +c.getArea());
		   System.out.println("Perimeter of circle is: " +c.getPerimeter());
		   System.out.println("Radius os circle is: " +c.getRadius());
	   }
}
