package g30124.panta.liviu.lab5.ex3;

import java.util.Random;
import static java.lang.Math.abs;

public class TemperatureSensor extends Sensor{

	private Random temp = new Random(100);
	
	public TemperatureSensor(String location) {
		super(location);
	}
	
	@Override
	public int readValue() {
		return abs(temp.nextInt()%100);
	}
	
}
