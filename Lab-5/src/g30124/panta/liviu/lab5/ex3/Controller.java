package g30124.panta.liviu.lab5.ex3;

import java.util.concurrent.TimeUnit;

public class Controller {

	private Sensor lum = new LightSensor("dormitor");
	private Sensor tem = new TemperatureSensor("bucatarie");
	
	public void display() {
		for(int i=0;i<20;i++) {
			try {
				TimeUnit.SECONDS.sleep(1);
				System.out.println("Light sensor: " +lum.readValue());
				System.out.println("Temperature sensor: " +tem.readValue());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) {
		Controller c = new Controller();
		c.display();
	}
}
