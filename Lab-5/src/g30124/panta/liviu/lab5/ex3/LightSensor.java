package g30124.panta.liviu.lab5.ex3;

import java.util.Random;
import static java.lang.Math.abs;

public class LightSensor extends Sensor{

	private Random light = new Random(100);
	
	public LightSensor(String location) {
		super(location);
	}
	
	@Override
	public int readValue() {
		return abs(light.nextInt()%100);
	}
}
