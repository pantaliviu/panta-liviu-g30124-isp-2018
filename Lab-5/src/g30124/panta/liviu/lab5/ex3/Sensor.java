package g30124.panta.liviu.lab5.ex3;

public abstract class Sensor {

	protected String location;
	
	public Sensor(String location) {
		this.location = location;
	}
	
	public abstract int readValue();
	
	public String getLocation() {
		return this.location;
	}
}
