package g30124.panta.liviu.lab5.ex2;

public class ProxyImage implements Image{
	 
	   private Image image;
	   private String fileName;
	 
	   public ProxyImage(String fileName, boolean real_rotated){
	      this.fileName = fileName;
	      if(real_rotated == true)
	    	  this.image  = new RotatedImage(this.fileName);
	      else
	    	  this.image = new RealImage(this.fileName);
	   }
 
	   @Override
	   public void display() {
	      image.display();
	   }

	   public static void main(String[] args) {
		  ProxyImage picture = new ProxyImage("masina",false);
		  picture.display();
	   }
	}