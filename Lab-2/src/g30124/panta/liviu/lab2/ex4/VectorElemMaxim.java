package g30124.panta.liviu.lab2.ex4;

import java.util.Scanner;

public class VectorElemMaxim {
	
	static int max(int a,int b){
		if(a>b)
			return a;
		else return b;
	}
	
	public static void main(String[] args){
		
		int i,n;
		int[] x = null;
		
		Scanner in = new Scanner(System.in);
		System.out.println("Dati numarul de elemente al vectorului n = ");
		n = in.nextInt();
		x = new int[n];
		
		for(i=0; i<n; i++)
			{
			System.out.println("Elementul "+i+" din vector este: ");
			x[i] = in.nextInt(); 
			}
		in.close();
		
		int maxim = x[0];
		
		for(i=0; i<n; i++)
			if(max(x[i],maxim)==x[i])
				maxim=max(x[i],maxim);
		System.out.println("Maximul din vectorul dat este: " +maxim);
	}
}
