package g30124.panta.liviu.lab2.ex6;

import java.util.Scanner;

public class NrFactorial {
	
	//metoda nerecursiva
	static int fact1(int n){
		int i,s=1;
		for(i=1; i<=n;i++)
			s*=i;
		return s;
	}
	
	//metoda recursiva
	static int fact2(int n){
		if (n==1) 
			return 1;
		else 
			return n*fact2(n-1);
	}
	
	public static void main(String[] args){
		int n;
		
		Scanner in = new Scanner(System.in);
		System.out.println("Dati n = ");
		n = in.nextInt();
		in.close();
		
		System.out.println("Factorialul lui n calculat nerecursiv: "+fact1(n));
		System.out.println("Factorialul lui n calculat recursiv: "+fact2(n));
	}
}
