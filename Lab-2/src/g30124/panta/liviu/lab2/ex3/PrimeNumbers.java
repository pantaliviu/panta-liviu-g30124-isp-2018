package g30124.panta.liviu.lab2.ex3;

import java.util.Scanner;

public class PrimeNumbers {
	
	static int prim(int n){
		int i,s=0;
		for(i=1; i<=n; i++)
			if(n%i==0)
				s++;
		return s;
	}
	
	public static void main(String[] args){
		
		int i,s=0;
		int a,b;
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Dati a: ");
		a = in.nextInt();
		
		System.out.println("Dati b: ");
		b = in.nextInt();
		
		in.close();
		
		for(i=a; i<=b; i++)
			if(prim(i)==2){
				System.out.printf("%d ",i);
				s++;
			}
		System.out.println("Numarul de numere prime din intervalul dat este: "+s);
	}
	

}
