package g30124.panta.liviu.lab10.ex3;

public class CounterFrom0To200 extends Thread{

	String n;
    Thread t;

    CounterFrom0To200(String n, Thread t){
        this.n = n;this.t=t;
    }

    public void run()
    {
        System.out.println("Firul "+n+" a intrat in metoda run()");
        try
        {
            if (t!=null) {
                t.join();

                for(int i=100; i<200; i++){
                    try{
                        System.out.println(n+ " este la pozitia : " +(i+1));
                        Thread.sleep(100);
                    } catch(Exception e){e.printStackTrace();}
                }
                System.out.println(n + " is finished!");
            }

            else{

                for(int i=0; i<100; i++){
                    try{
                        System.out.println(n+ " este la pozitia : " +(i+1));
                        Thread.sleep(200);
                    } catch(Exception e){e.printStackTrace();}
                }
                	System.out.println(n + " is finished!");
            }

        }
        catch(Exception e){e.printStackTrace();}

    }

    public static void main(String[] args)
    {
        CounterFrom0To200 w1 = new CounterFrom0To200("Proces 1",null);
        CounterFrom0To200 w2 = new CounterFrom0To200("Proces 2",w1);
        w1.start();
        w2.start();
    }

}
