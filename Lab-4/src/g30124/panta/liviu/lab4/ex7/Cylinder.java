package g30124.panta.liviu.lab4.ex7;

import g30124.panta.liviu.lab4.ex3.*;

public class Cylinder extends Circle{
	
	private double height = 1.0;
	
	public Cylinder() {
		
		this.height = 6;
		this.radius = 2;
	}
	
	public Cylinder(double radius) {
		
		super(radius);

	}
	
	public Cylinder(double radius,double height) {
		
		super(radius);
		this.height = height;
	}
	
	public double getHeight() {
		return this.height;
	}
	
	public double getVolume() {
		return getArea()*height;
	}
	
	public static void main(String[] args) {
		
		Cylinder c = new Cylinder(2.0,9.0);
		System.out.println("Height of cylinder is: " +c.getHeight());
		System.out.println("Volume of cylinder is: " +c.getVolume());
	}

}
