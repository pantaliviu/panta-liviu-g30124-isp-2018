package g30124.panta.liviu.lab4.ex7;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TestMyCylinder {

	@Test
	public void TestVolume() {
		Cylinder c = new Cylinder(2.0,9.0);
		assertTrue(c.getVolume() == 113.04);
	}
	
	@Test
	public void TestHeight() {
		Cylinder c = new Cylinder(2.0,9.0);
		assertTrue(c.getHeight() == 9.0);
	}
	
	@Test
	public void TestRadius() {
		Cylinder c = new Cylinder(2.0,9.0);
		assertTrue(c.getRadius() == 2.0);
	}
}
