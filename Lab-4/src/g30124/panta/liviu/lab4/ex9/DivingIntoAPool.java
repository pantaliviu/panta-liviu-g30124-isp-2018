package g30124.panta.liviu.lab4.ex9;

import becker.robots.*;

public class DivingIntoAPool extends Robot {
	
	public DivingIntoAPool(City City, int Street, int Avenue, Direction Direction) {
		
		super(City,Street,Avenue,Direction);
	}
	
	public void TurnAround() {
		this.turnLeft();
		this.turnLeft();
		this.turnLeft();
	}
	
	public void Dive() {
		for(int i=0;i<4;i++)
			this.turnLeft();
		this.move();
		for(int i=0;i<4;i++)
			this.turnLeft();
		this.move();
		for(int i=0;i<4;i++)
			this.turnLeft();
		this.move();
	}
	
	public static void main(String[] args) {
		
		City cluj = new City();
		DivingIntoAPool kevin = new DivingIntoAPool(cluj,2,2,Direction.NORTH);
		Wall zid1 = new Wall(cluj,3,2,Direction.NORTH);
		Wall zid2 = new Wall(cluj,3,1,Direction.EAST);
		Wall zid3 = new Wall(cluj,4,1,Direction.EAST);
		Wall zid4 = new Wall(cluj,5,1,Direction.EAST);
		Wall zid5 = new Wall(cluj,5,2,Direction.WEST);
		Wall zid6 = new Wall(cluj,5,2,Direction.SOUTH);
		Wall zid7 = new Wall(cluj,5,3,Direction.EAST);
		Wall zid8 = new Wall(cluj,5,3,Direction.SOUTH);
		kevin.move();
		kevin.TurnAround();
		kevin.move();
		kevin.TurnAround();
		kevin.move();
		kevin.Dive();
		kevin.turnLeft();
		kevin.turnLeft();
	}

}
