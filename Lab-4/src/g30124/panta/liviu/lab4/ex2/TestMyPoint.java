package g30124.panta.liviu.lab4.ex2;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestMyPoint {
	
	@Test
	public void setXY(){
		
		MyPoint m = new MyPoint();
		MyPoint n = new MyPoint(3,4);
		m.setX(3);
		assertEquals(m.getX(),n.getX());
		
	}
		
		
	@Test
	public void setY() {
		
		MyPoint m = new MyPoint();
		MyPoint n = new MyPoint(2,2);
		m.setY(2);
		assertEquals(m.getY(),n.getY());
		
	}
		
	@Test
	public void Distance() {
		
		MyPoint m = new MyPoint();
		assertTrue(m.distance(3,6) == 3);
	}

}
