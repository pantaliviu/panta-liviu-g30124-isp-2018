package g30124.panta.liviu.lab4.ex4;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestAuthor {
	
	@Test
	public void verifyName() {
		
		Author a = new Author("Mihai Eminescu","Eminescu50@yahoo.com",'m');
		assertTrue(a.getName() == "Mihai Eminescu");
		
	}
	
	@Test
	public void verifyEmail() {
		
		Author a = new Author("Mihai Eminescu","Eminescu50@yahoo.com",'m');
		a.setEmail("Mihai-Eminescu@gmail.com");
		assertTrue(a.getEmail() == "Mihai-Eminescu@gmail.com");
		
	}
	
	@Test
	public void verifyGender() {
		
		Author a = new Author("Mihai Eminescu","Eminescu50@yahoo.com",'m');
		assertTrue(a.getGender() == 'm');
		
	}

}
