package g30124.panta.liviu.lab4.ex4;

public class Author {
	
	private String name;
	private String email;
	private char gender;
	
	public Author(String name, String email, char gender) {
		
		this.name = name;
		this.email = email;
		this.gender = gender;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		 this.email = email;
	}
	
	public char getGender() {
		return gender;
	}
	
	public String toString() {
		return new String("Autor-"+this.name+" ("+this.gender+") la "+this.email);
	}
	
	
	public static void main(String[] args) {
		
		Author a = new Author("Mihai Eminescu","Eminescu50@yahoo.com",'m');
		a.setEmail("Mihai_Eminescu@gmail.com");
		System.out.println("Name of author is: " +a.getName());
		System.out.println("Gender of author is: " +a.getGender());
		System.out.println("Email of author is: " +a.getEmail());
		System.out.print(a.toString());
	}

}
