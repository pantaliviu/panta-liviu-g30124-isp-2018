package g30124.panta.liviu.lab4.ex1;

public class Conveyor {

    private Box[] packages = new Box[30];

    void addPackage(Box p, int pos){
        packages[pos] = p;
    }

    public void moveLeft(){
        for(int i=0;i<packages.length-1;i++){
            packages[i] = packages[i+1];
        }
        packages[packages.length-1] = null;

    }

    public void moveRight() {
        for (int i = packages.length - 1; i > 0; i--) {
            packages[i] = packages[i - 1];
        }
        packages[0] = null;
    }
    
    public Box pickBox(int pos) {
    	packages[pos] = null;
    	return packages[pos];
    }

    public Box getBox(int pos){
        return packages[pos];
    }

    public void display(){
        int k = 0;
        for (int i = 0; i < packages.length; i++) {
            String c = (packages[i]!=null)?packages[i].toString():"_";
            System.out.print(c);
        }
        System.out.println();
    }
}
