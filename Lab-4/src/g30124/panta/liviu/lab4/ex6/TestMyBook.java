package g30124.panta.liviu.lab4.ex6;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import g30124.panta.liviu.lab4.ex4.Author;

public class TestMyBook {

	@Test
	public void TestName() {
		Author[] author = new Author[1];
		author[0] = new Author("Ion Creanga","Ion_Creanga@gmail.com",'m');
		Book b = new Book("Amintiri din copilarie",author,15,30);
		assertTrue(b.getName() == "Amintiri din copilarie");
	}
	
	@Test
	public void TestQuantity() {
		Author[] author = new Author[1];
		author[0] = new Author("Ion Creanga","Ion_Creanga@gmail.com",'m');
		Book b = new Book("Amintiri din copilarie",author,15,30);
		b.setQtyInStock(500);
		assertTrue(b.getQtyInStock() == 500);
	}
	
	@Test
	public void TestPrice() {
		Author[] author = new Author[1];
		author[0] = new Author("Ion Creanga","Ion_Creanga@gmail.com",'m');
		Book b = new Book("Amintiri din copilarie",author,15,30);
		b.setPrice(40);
		assertTrue(b.getPrice() == 40);
	}
}
