package g30124.panta.liviu.lab4.ex6;

import g30124.panta.liviu.lab4.ex4.*;

public class Book {
	
	private String name;
	private double price;
	private int qtyInStock = 0;
	private Author[] author;
	
	public Book(String name,Author[] author, double price) {
		
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public Book(String name,Author[] author,double price,int qtyInStock) {

		this.name = name;
		this.price = price;
		this.author = author;
		this.qtyInStock = qtyInStock;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Author[] getAuthor() {
		return this.author;
	}
	
	public void printAuthors(Author[] author) {
		for(int i=0;i<author.length;i++)
		System.out.println(author[i].getName());
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public String toString() {
		return new String(getName()+" by "+author.length+" authors");
	}

	public static void main(String[] args) {
		
		Author[] author = new Author[4];
		author[0] = new Author("Mihai Eminescu", "Eminescu50@yahoo.com", 'm');
		author[1] = new Author("Ion Creanga", "Ion_Creanga@yahoo.com", 'm');
		author[2] = new Author("Mihail Sadoveanu", "Sadoveanu@yahoo.com", 'm');
		author[3] = new Author("Liviu Rebreanu", "Glasul_pamantului@yahoo.com", 'm');
		Book b = new Book("Luceafarul",author,20.5,500);
		System.out.println("Price of a book is: " +b.getPrice());
		System.out.println("Name of book is:" +b.getName());
		System.out.println("Quantity in stock is: " +b.getQtyInStock());
		b.printAuthors(author);
		System.out.println(b.toString());
	}
}
