package g30124.panta.liviu.lab4.ex8;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestMyShapes {

	@Test
	public void TestColor() {
		Shape s = new Shape("Blue",false);
		s.setColor("Red");
		assertEquals(s.getColor(),"Red");
	}
	
	@Test
	public void TestValue() {
		Shape s = new Shape("Red",false);
		assertEquals(s.isFilled(),false);
	}
	
	@Test
	public void TestPerimeterCircle() {
		Circle c = new Circle(3.0);
		assertTrue(c.getPerimeter() == 18.84);
	}
	
	@Test
	public void TestAreaCircle() {
		Circle c = new Circle(3.0);
		assertTrue(c.getArea() == 28.259999999999998);
	}
	
	@Test
	public void TestRadiusCircle() {
		Circle c = new Circle(3.0);
		assertTrue(c.getRadius() == 3.0);
	}
	
	@Test
	public void TestRectangelWidth() {
		Rectangle r = new Rectangle(5.0,2.0);
		assertTrue(r.getWidth() == 5.0);
	}
	
	@Test
	public void TestRectangleLength() {
		Rectangle r = new Rectangle(5.0,2.0);
		assertTrue(r.getLength() == 2.0);
	}
	
	@Test
	public void TestRectangleArea() {
		Rectangle r = new Rectangle(5.0,2.0);
		assertTrue(r.getArea() == 10.0);
	}
	
	@Test
	public void TestRectanglePerimeter() {
		Rectangle r = new Rectangle(5.0,2.0);
		assertTrue(r.getPerimeter() == 14.0);
	}
	
	@Test
	public void TestSquareWidth() {
		Square s = new Square();
		s.setWidth(4.0);
		s.setLength(10.0);
		assertTrue(s.getWidth() == s.getLength());
	}
}
