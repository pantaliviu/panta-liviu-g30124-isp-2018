package g30124.panta.liviu.lab4.ex8;

public class Circle extends Shape {

	private double radius;
	
	public Circle() {
		
		this.radius = 1.0;
	}
	
	public Circle(double radius) {
		
		this.radius = radius;
	}
	
	public Circle(double radius, String color, boolean filled) {
		
		super(color,filled);
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getArea() {
		return 3.14*this.radius*this.radius;
	}
	
	public double getPerimeter() {
		return 2*3.14*this.radius;
	}
	
	@Override
	public String toString() {
		return new String("A Circle with radius="+getRadius()+" which is a subclass of "+super.toString()); 
	}
	
	public static void main(String[] args) {
		
		Circle c = new Circle(3.0,"blue",true);
		System.out.println("Perimeter of circle is: " +c.getPerimeter());
		System.out.println("Radius of circle is: " +c.getRadius());
		System.out.println("Area of circle is: " +c.getArea());
		System.out.println(c.toString());
	}
}
