package g30124.panta.liviu.lab4.ex3;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestCircle {

	@Test
	public void verifyArea() {
		Circle c = new Circle(2.0);
		assertTrue(c.getArea() == 12.56);
	}
	
	@Test
	public void verifySet() {
		
		Circle c = new Circle(3.4);
		assertTrue(c.getRadius() == 3.4);
	}
}
