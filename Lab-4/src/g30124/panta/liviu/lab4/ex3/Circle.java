package g30124.panta.liviu.lab4.ex3;

public class Circle {

	protected double radius;
	private String color;
	
	public Circle(){
		this.radius = 1.0;
		this.color = "red";
	}
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		return 3.14*this.radius*this.radius;
	}
	
	public static void main(String[] args) {
		Circle c = new Circle(2.0);
		System.out.println("Radius of circle is: " +c.getRadius());
		System.out.print("Area of circle is: " +c.getArea());
	}
}
