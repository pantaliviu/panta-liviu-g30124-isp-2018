package g30124.panta.liviu.lab4.ex5;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import g30124.panta.liviu.lab4.ex4.Author;

public class TestMyBook {
	
	@Test
	public void testGetPrice() {
		Book b = new Book("Amintiri din copilarie",null, 20.5);
		assertTrue(b.getPrice() == 20.5);
	}
	
	@Test
	public void testGetQtyInStock() {
		Book b = new Book("Amintiri din copilarie",null, 20.5,100);
		assertTrue(b.getQtyInStock() == 100);
	}
	
	@Test
	public void getAuthor() {
		Author author = new Author("Ion Creanga","IonCreanga@yahoo.com",'m');
		Book b = new Book("Amintiri din copilarie",author, 20.5);
		assertEquals(b.getAuthor(),author.getName());
	}
	
	@Test
	public void testBookName() {
		Book b = new Book("Amintiri din copilarie",null, 20.5);
		assertTrue(b.getName() == "Amintiri din copilarie");
	}

}
