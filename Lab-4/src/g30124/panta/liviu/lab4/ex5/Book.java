package g30124.panta.liviu.lab4.ex5;

import g30124.panta.liviu.lab4.ex4.*;

public class Book {

	private String name;
	private double price;
	private int qtyInStock = 0;
	private Author author;
	
	public Book(String name,Author author, double price) {
		
		this.name = name;
		this.author = author;
		this.price = price;	
	}
	
	public Book(String name,Author author,double price,int qtyInStock) {

		this.name = name;
		this.price = price;
		this.author = author;
		this.qtyInStock = qtyInStock;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getAuthor() {
		return author.getName();
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public String toString() {
		return new String("'"+getName()+"' by "+author.getName()+" ("+author.getGender()+") at "+author.getEmail());
	}
	
	public static void main(String[] args) {
		
		Author author = new Author("Mihai Eminescu", "Eminescu50@yahoo.com", 'm');
		Book b = new Book("Luceafarul",author,20.5);
		b.setQtyInStock(200);
		System.out.println("Name of book is: " +b.getName());
		System.out.println("Name of author is: " +b.getAuthor());
		System.out.println("Number of books in stock is: " +b.getQtyInStock());
		System.out.println("Price of a book is: " +b.getPrice());
		System.out.println(b.toString());
	}
}
